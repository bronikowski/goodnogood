
import simplejson, requests, pickle

def parse(url):

    items = {}

    result = requests.get('%s' % url)
    try:
        dejson = simplejson.loads(result.text)
    except simplejson.decoder.JSONDecodeError:
        print 'Failed to decode JSON @ %s' % url
        return ({},None)
    print 'Loading %s (after:%s, before: %s)' % (url, dejson['data']['after'], dejson['data']['before'])
    for d in dejson['data']['children']:
        items[ d['data']['url']] = d['data']['title']
    return (items, dejson['data']['after'])

def cache_it(dictionary,file_name):
    fh = file(file_name,'wb')
    fh.write(pickle.dumps(dictionary))
    fh.close()

def expand_it(file_name):
    try:
        fh = file(file_name,'rb')
        return pickle.loads(fh.read())
    except IOError:
        return {}
