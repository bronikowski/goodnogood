from engine import parse, expand_it, cache_it

NO_SOB = [ 
	'http://www.reddit.com/r/no_sob_story/top/.json',
	'http://www.reddit.com/r/no_sob_story/.json',
	'http://www.reddit.com/r/rising/.json',
	'http://www.reddit.com/r/no_sob_story/new/.json',
	'http://www.reddit.com/r/no_sob_story/hot/.json',
     ]
PICS = [
        'http://www.reddit.com/r/pics/top/.json',
        'http://www.reddit.com/r/pics/rising/.json',
        'http://www.reddit.com/r/pics/new/.json',
        'http://www.reddit.com/r/pics/hot/.json',
        'http://www.reddit.com/r/pics/.json',
     ]

no_sob = expand_it('no_sob_story.pickle')
pics = expand_it('pics.pickle')

for url in NO_SOB:
    data = parse(url)
    no_sob.update(data[0])
    while data[1]:
        data = parse('%s?after=%s' % (url,data[1]))
        no_sob.update(data[0])

cache_it(no_sob,'no_sob_story.pickle')
for url in PICS:
    data = parse(url)
    pics.update(data[0])
    while data[1]:
        data = parse('%s?after=%s' % (url,data[1]))
        pics.update(data[0])

cache_it(pics,'pics.pickle')
difference = []
for key, ns in no_sob.items():
    try:
        print "No sob:%s\nOriginal:%s\n" % (ns, pics[key])
        difference.append({'url':key, 'sob': pics[key], 'no_sob':ns})
    except KeyError:
        pass

cache_it(difference,'difference.pickle')
